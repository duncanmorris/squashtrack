import argparse
import cv2
import imutils
import numpy as np

from utils import show_image
from utils import find_likely_balls
from utils import guess_ball_from_potential_balls


def process_video(path_to_video):
    """Process the given video"""

    most_likely_ball = None

    background = cv2.createBackgroundSubtractorKNN(
        history=400, dist2Threshold=200, detectShadows=False
    )

    cap = cv2.VideoCapture(path_to_video)
    if not cap.isOpened():
        raise Exception("Couldn't open video {v}".format(v=path_to_video))

    fourcc = cv2.VideoWriter_fourcc("m", "p", "4", "v")
    fps = 20
    video_out = cv2.VideoWriter("tracked.mov", fourcc, fps, (500, 281), True)

    while cap.isOpened():
        ret, frame = cap.read()
        if not ret:
            break

        frame = imutils.resize(frame, width=500)

        # Mask the timestamp in the tlh corner
        mask = np.ones(frame.shape[:2], dtype=np.uint8)
        cv2.rectangle(mask, (12, 0), (130, 10), (0), thickness=-1)
        frame = cv2.bitwise_and(frame, frame, mask=mask)

        foreground = background.apply(frame)

        _, threshold = cv2.threshold(foreground, 20, 255, cv2.THRESH_BINARY)

        show_image(threshold, "undilated", 0, 281)

        kernel = np.ones((2, 2), np.uint8)
        eroded = cv2.erode(threshold, kernel, iterations=1)
        dilated = cv2.dilate(eroded, kernel, iterations=4)

        potential_balls = find_likely_balls(dilated)
        guessed_balls = guess_ball_from_potential_balls(
            potential_balls, most_likely_ball
        )
        if len(guessed_balls) == 1:
            most_likely_ball == guessed_balls[0]
            cv2.drawContours(frame, potential_balls, -1, (255, 0, 0), 2)

        show_image(frame, "frame", 0, 0)
        show_image(dilated, "dilated", 500, 0)

        video_out.write(frame)

        if cv2.waitKey(10) == ord("q"):
            cv2.imwrite("frame.png", frame)
            cv2.imwrite("threshold.png", threshold)
            cv2.imwrite("dilated.png", dilated)
            break

    cap.release()
    video_out.release()
    cv2.destroyAllWindows()


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Process the given video")

    parser.add_argument("path_to_video", help="(relative) path to video file")

    args = parser.parse_args()

    process_video(args.path_to_video)
