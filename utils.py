import cv2


def show_image(image, name, pos_x, pos_y):
    """Show the given image at the given position"""

    cv2.namedWindow(name)
    cv2.moveWindow(name, pos_x, pos_y)
    cv2.imshow(name, image)


def find_likely_balls(image):
    """Find and return any contours from the given image which could be balls"""

    _, contours, _ = cv2.findContours(image, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    balls = [contour for contour in contours if could_be_ball(contour)]

    if len(balls) == 0:
        return []

    if len(balls) > 10:
        # Too many balls - abort
        return []

    return balls


def could_be_ball(contour):
    """Return whether we think this could be a squash ball, based on the contour"""

    x, y, w, h = cv2.boundingRect(contour)
    _, radius = cv2.minEnclosingCircle(contour)

    width_ok = 5 <= w <= 20
    height_ok = 5 <= h <= 20

    width_ok = True
    height_ok = True
    radius_ok = 2 <= radius <= 10
    return all([width_ok, height_ok, radius_ok])


def guess_ball_from_potential_balls(potential_balls, most_likely_ball):
    """Guess which of the potential balls is the most likely to be the real ball

        if most_likely_ball is set we'd expect it to be close to it

    """

    if most_likely_ball is not None:
        return [
            b for b in potential_balls if distance_between(most_likely_ball, b) < 30
        ]

    return potential_balls


def distance_between(contour1, contour2):

    (x1, y1), _ = cv2.minEnclosingCircle(contour1)
    (x2, y2), _ = cv2.minEnclosingCircle(contour2)
    dist = math.hypot(x2 - x1, y2 - y1)

    return dist
